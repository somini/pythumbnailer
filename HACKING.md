## Development

To generate the test folder in `tests/`, use:

```sh
make test
```

This should show all features available on the program. This should be re-run when the HTML templates change.

To edit the static files in `pythumbnailer/static`, the can be edited directly and the page reloaded, no need to re-run anything.

To run a server properly configured for tests, follow the instructions on:

```sh
make run-server
```

This default to binding to `localhost`, but this can be configured with the `SERVER-BIND` Make variable. To listen on all interfaces (that is, exposing the server to all the interfaces), use:

```sh
make run-server SERVER-BIND=0.0.0.0
```
