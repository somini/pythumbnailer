#!/bin/sh
set -e

STATIC_FOLDER="pythumbnailer/static/lib"

# Versions
LEAFLET_VERSION=1.8.0
LEAFLET_ELEVATION_VERSION=1.9.0
LEAFLET_GPX_VERSION=1.7.0
D3_VERSION=7.6.1

elevation_theme="steelblue"

set -x

# Leaflet
curl "https://unpkg.com/leaflet@$LEAFLET_VERSION/dist/leaflet.js" >"$STATIC_FOLDER/leaflet@$LEAFLET_VERSION.min.js"
curl "https://unpkg.com/leaflet@$LEAFLET_VERSION/dist/leaflet.css" >"$STATIC_FOLDER/leaflet@$LEAFLET_VERSION.css"
# Leaflet Elevation
curl "https://unpkg.com/@raruto/leaflet-elevation@$LEAFLET_ELEVATION_VERSION/images/elevation.svg" >"$STATIC_FOLDER/../images/elevation-$elevation_theme.svg"
curl "https://unpkg.com/@raruto/leaflet-elevation@$LEAFLET_ELEVATION_VERSION/dist/leaflet-elevation.min.js" >"$STATIC_FOLDER/leaflet-elevation@$LEAFLET_ELEVATION_VERSION.min.js"
curl "https://unpkg.com/@raruto/leaflet-elevation@$LEAFLET_ELEVATION_VERSION/dist/leaflet-elevation.min.css" >"$STATIC_FOLDER/leaflet-elevation@$LEAFLET_ELEVATION_VERSION.min.css"
# Leaflet GPX
curl "https://cdnjs.cloudflare.com/ajax/libs/leaflet-gpx/$LEAFLET_GPX_VERSION/gpx.min.js" >"$STATIC_FOLDER/leaflet-gpx@$LEAFLET_GPX_VERSION.min.js"
# D3
curl "https://cdnjs.cloudflare.com/ajax/libs/d3/$D3_VERSION/d3.min.js" >"$STATIC_FOLDER/d3@$D3_VERSION.min.js"
