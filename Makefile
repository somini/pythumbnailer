PY := python3.8
include Makefile.venv

.PHONY: release
release: | $(VENV)
	$(RM) -r build/
	$(VENV)/python setup.py sdist

.PHONY: test
test: | $(VENV)
	# "Test", just generate the test folder
	cd tests && ../$(VENV)/pypublish -v \
		--insecure --static-location ../pythumbnailer/static/

.PHONY: run-server
SERVER-BIND := localhost
run-server: test | $(VENV)
	# Point your browser at "http://$(SERVER-BIND):8888/tests/index.html"
	@$(VENV)/python -m http.server 8888 --bind $(SERVER-BIND)
